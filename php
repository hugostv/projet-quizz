<?php

// Set up the quizz questions and answers
$quiz = array(
    array(
        "question" => "What is the capital of France?",
        "answers" => array("Paris", "London", "Rome", "Berlin"),
        "correct" => "Paris"
    ),
    array(
        "question" => "",
        "answers" => ,
        "correct" => ""
    ),
    array(
        "question" => "",
        "answers" => ,
        "correct" => ""
    )
);

// Check if the form has been submitted
if (isset($_POST["submit"])) {
    // Initialize the quiz score
    $score = 0;
    
    // Loop through the quiz questions
    foreach ($quiz as $question) {
        // Check if the user's answer is correct
        if ($_POST[$question["question"]] == $question["correct"]) {
            // Increment the score if the answer is correct
            $score++;
        }
    }
    
    // Display the quiz results
    echo "You scored " . $score . " out of " . count($quiz) . "!";
    
} else {
    // Display the quiz form
    echo "<form method='post' action=''>";
    
    // Loop through the quiz questions
    foreach ($quiz as $question) {
        // Display the question
        echo "<p>" . $question["question"] . "</p>";
        
        // Loop through the answers
        foreach ($question["answers"] as $answer) {
            // Display the answer as a radio button
            echo "<input type='radio' name='" . $question["question"] . "' value='" . $answer . "'>" . $answer . "<br>";
        }
    }
    
    // Add a submit button
    echo "<input type='submit' name='submit' value='Submit'>";
    echo "</form>";
}